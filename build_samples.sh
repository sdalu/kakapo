#!/bin/sh

if [ -z "$1" ]; then
    name="buzzer_melody"
else
    name="$1"
fi

mkdir -p build
rm -f "build/${name}.c"
for f in samples/*.beep ; do
    ruby kakapo.rb -w "${name}" "$f" >> "build/${name}.c"
    cat "build/${name}.c" | grep char | sed 's/ = {/;/' | sed 's/^/extern /' > "build/${name}.h"
done

       

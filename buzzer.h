/*
    Copyright (C) 2016 Stephane D'Alu

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef _BUZZER_H_
#define _BUZZER_H_


/*======================================================================*
 * Compile time                                                         *
 *======================================================================*/

#ifndef BUZZER_USE_MELODY
#define BUZZER_USE_MELODY TRUE
#endif



/*======================================================================*
 * Constantes                                                           *
 *======================================================================*/

#define BUZZER_NOTE_A                0  /**< Note A */
#define BUZZER_NOTE_B                1  /**< Note B */
#define BUZZER_NOTE_C                2  /**< Note C */
#define BUZZER_NOTE_D                3  /**< Note D */
#define BUZZER_NOTE_E                4  /**< Note E */
#define BUZZER_NOTE_F                5  /**< Note F */
#define BUZZER_NOTE_G                6  /**< Note G */

#define BUZZER_NOTE_FLAT          (-1)  /**< Accidental Flat (Bemole) */
#define BUZZER_NOTE_NATURAL        (0)  /**< Accidental Natural       */
#define BUZZER_NOTE_SHARP          (1)  /**< Accidental Sharp (Diese) */

#define BUZZER_NOTE_LARGE                          3 /**< Note length: 8/1   */
#define BUZZER_NOTE_LONG                           2 /**< Note length: 4/1   */
#define BUZZER_NOTE_BREVE                          1 /**< Note length: 2/1   */
#define BUZZER_NOTE_SEMIBREVE                      0 /**< Note length: 1/1   */
#define BUZZER_NOTE_MINIM                         -1 /**< Note length: 1/2   */
#define BUZZER_NOTE_CROTCHET                      -2 /**< Note length: 1/4   */
#define BUZZER_NOTE_QUAVER                        -3 /**< Note length: 1/8   */
#define BUZZER_NOTE_SEMIQUAVER                    -4 /**< Note length: 1/16  */
#define BUZZER_NOTE_DEMISEMIQUAVER                -5 /**< Note length: 1/32  */
#define BUZZER_NOTE_HEMIDEMISEMIQUAVER            -6 /**< Note length: 1/64  */
#define BUZZER_NOTE_SEMIHEMIDEMISEMIQUAVER        -7 /**< Note length: 1/128 */
#define BUZZER_NOTE_DEMISEMIHEMIDEMISEMIQUAVER    -8 /**< Note length: 1/256 */

#define BUZZER_TONE_C                0  /**< Tone corresponding to C  */
#define BUZZER_TONE_CS               1  /**< Tone corresponding to C# */
#define BUZZER_TONE_D                2  /**< Tone corresponding to D  */
#define BUZZER_TONE_DS               3  /**< Tone corresponding to D# */
#define BUZZER_TONE_E                4  /**< Tone corresponding to E  */
#define BUZZER_TONE_F                5  /**< Tone corresponding to F  */
#define BUZZER_TONE_FS               6  /**< Tone corresponding to F# */
#define BUZZER_TONE_G                7  /**< Tone corresponding to G  */
#define BUZZER_TONE_GS               8  /**< Tone corresponding to G# */
#define BUZZER_TONE_A                9  /**< Tone corresponding to A  */
#define BUZZER_TONE_AS              10  /**< Tone corresponding to A# */
#define BUZZER_TONE_B               11  /**< Tone corresponding to B  */

#define BUZZER_HZ_MIN               20  /**< Minimum pitch */
#define BUZZER_HZ_MAX            20000  /**< Maximum pitch */

#define BUZZER_LENGTH_MIN            1  /**< Minimum sound length         */
#define BUZZER_LENGTH_MAX         8000  /**< Maximum sound length         */
#define BUZZER_LENGTH_INFINITE    (-1)  /**< Sound is considered infinite */

#define BUZZER_DELAY_MIN             1  /**< Minimum delay between sounds */
#define BUZZER_DELAY_MAX         60000  /**< Maximum delay between sounds */

#define BUZZER_COUNT_MIN             1  /**< Minimum occurences of the sound */
#define BUZZER_COUNT_MAX         50000  /**< Maximum occurenced of the sound */
#define BUZZER_COUNT_INFINITE     (-1)  /**< Sound is repeated indefinitely  */

#define BUZZER_TYPE_BUZZ             0
#define BUZZER_TYPE_BEEP             1
#define BUZZER_TYPE_RING             2
#define BUZZER_TYPE_MELODY           3


/*======================================================================*
 * Structures                                                           *
 *======================================================================*/

typedef struct {
    PWMDriver      *pwm;
    pwmchannel_t    channel;    
} BuzzerConfig;



typedef struct buzzer_ring_ctx {
    uint16_t hz;             /**< sound pitch in hz     */
    uint16_t length;         /**< sound lenght in ms    */
    uint16_t count;          /**< sound occurence       */
    uint16_t delay;          /**< delay beetween sounds */
    bool     playing;        /**< is emitting "noise"   */
} *buzzer_ring_ctx_t;

#if BUZZER_USE_MELODY
typedef struct buzzer_melody_ctx {
    uint8_t  state;         /**< melody player state          */
    uint8_t  bpm;           /**< current bpm                  */
    uint8_t  ulength;       /**< current length unit (in ms)  */
    uint8_t  volume;        /**< sound volume                 */
    uint16_t dflt_silence;  /**< default silence (in ms)      */
    uint16_t silence;       /**< silence to be played (in ms) */
    char    *next;          /**< remaining melody to play     */
} *buzzer_melody_ctx_t;
#endif

typedef struct buzzer {
    uint8_t type;          /**< buzzing type                 */
    volatile bool done;    /**< is buzzing sequence done?    */
    virtual_timer_t vt;    /**< virtual timer for scheduling */
    union {
	struct buzzer_ring_ctx   ring;    /**< context for Ring type   */
#if BUZZER_USE_MELODY
	struct buzzer_melody_ctx melody;  /**< contect for Melody type */
#endif
    };
    const BuzzerConfig *config;  /**< buzzer configuration */
} *buzzer_t;



void buzzer_init(buzzer_t buzzer, const BuzzerConfig *cfg);


uint16_t buzzer_convert_tone(int8_t tone, int8_t octave);

void buzzer_off(buzzer_t buzzer);


void buzzer_ring(buzzer_t buzzer,
		 uint16_t hz,    uint16_t length,
		 uint16_t count, uint16_t delay);

/**
 * @brief Play a beep
 *
 * @param buzzer      Buzzer context
 * @param hz          Pitch in HZ
 * @param length      Sound length in milliseconds
 */
static inline
void buzzer_beep(buzzer_t buzzer,
		 uint16_t hz, uint16_t length) {
    buzzer_ring(buzzer, hz, length, 1, 0);
}

/**
 * @brief Buzz (no automatic turn off)
 *
 * @param buzzer      Buzzer context
 * @param hz          Pitch in HZ
 */
static inline
void buzzer_buzz(buzzer_t buzzer,
		 uint16_t hz) {
    buzzer_ring(buzzer, hz, BUZZER_LENGTH_INFINITE, 1, 0);
}


void buzzer_note(buzzer_t buzzer,
		 uint8_t note, int8_t accidental, int8_t octave,
		 uint8_t bpm,  int8_t value,      uint8_t dot);


void buzzer_midi(buzzer_t buzzer, uint8_t number, uint16_t length);



#if BUZZER_USE_MELODY
void buzzer_melody(buzzer_t buzzer, char *melody);
#endif


#endif

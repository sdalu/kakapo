# kakapo
Simple embedded beep player

Encoder
=======
Encoder (requires ruby >= 2.3) is able to read CSV file with frequency/value row
or beep command line, it will generate a C byte array.
It will try to guess BPM value or a fixed unit length for efficiently encoding data

~~~~sh
kakapo file.beep 
~~~~

~~~~
Usage: kakapo file [options]
    -v, --[no-]verbose               Run verbosely
    -b, --bpm BPM                    Beat Per Minute
    -u, --ulen LENGTH                Unit length 1..200ms
    -s, --silence LENGTH             Inbetween silence
    -f, --format FORMAT              Output format
    -o, --output FILENAME            Output filename
    -w, --wrap PREFIX                Prefix for output
    -h, --help                       Prints this help
~~~~

# Embedded player

Play music from kakapo-encoded byte-array.

~~~~c
static const BuzzerConfig buzzer_cfg = {
    .pwm     = &PWMD1,
    .channel = 0,
    .volume  = 100   
};
struct buzzer buzzer;

char melody_zelda[] = {
    0xfa, 0xfa, 0xfd, 0x8d, 0x51, 0x02, 0x4a, 0x04, 
    0x4d, 0x02, 0x51, 0x02, 0x4a, 0x04, 0x4d, 0x02, 
    0x51, 0x01, 0x54, 0x01, 0x53, 0x02, 0x4f, 0x02, 
    0x4d, 0x01, 0x4f, 0x01, 0x51, 0x02, 0x4a, 0x02, 
    0x48, 0x01, 0x4c, 0x01, 0x4a, 0x83, 0x00, 0xff };

buzzer_init(&buzzer, &buzzer_cfg);
buzzer_melody(&buzzer, melody_zelda);
~~~~

# Byte code description


| hex | description | followed by data
|-----|-------------|------
|  FF | End of music streeam | none
|  FE | Set BPM (sorry no speedcore music) | 1 byte (0-255)
|  FD | Set default delay between sound. | silence value encoded
|  FC | Silence extension. |  1 byte (2bit used)
|  FB | volume             | 1 byte (1..100)
|  FA | unit length        | 1 byte (1..250)
|  .. | _reserved_
|  D0 | _reserved_
| ..  | _sound_            | sound encoding

## Silence value encoding
| binary                               | description                                    |
|--------------------------------------|------------------------------------------------|
| 00000000                             | 0
| 00xxxxyy                             | value=xxxx=-8..6 dot=yy=0..3 (bpm relative)
| 01bbbbbb bbbbbbbb                    | 14bits encoded silence in ms
| 01bbbbbb bbbbbbbb 11111100 000000aa  | 16bits encoded: aabbbbbb bbbbbbbb
| 1xxxxxxx                             | in 5ms units 


## Sound encoding:
### Frequnecy
| binary                               | description                                    |
|--------------------------------------|------------------------------------------------|
| 1yyyyyyy yyyyyyyy                    | frequency 1..20000
| 00000000                             | _not allowed_
| 0xxxxxxx                             | midi number 21..108

### Length
| binary                               | description                                    |
|--------------------------------------|------------------------------------------------|
| 1.......                             | explicit silence after the sound (will be silence value encoded)
| .1bbbbbb bbbbbbbb                    | length is coded on 14-bits (will be in ms)
| .0000000                             | zero length(forbidden)
| .0xxxxyy                             | length in value (-8..6) /dot (0..3) 



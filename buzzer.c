/*
    Copyright (C) 2016 Stephane D'Alu

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include <ch.h>
#include <hal.h>


// http://newt.phys.unsw.edu.au/jw/notes.html
// https://en.wikipedia.org/wiki/Scientific_pitch_notation
// https://graphics.stanford.edu/~seander/bithacks.html#FixedSignExtend

#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

#include "buzzer.h"

#define BUZZER_MELODY_SOUND   0
#define BUZZER_MELODY_SILENCE 1



static
void buzzer_do_next(void *args);

/*======================================================================*
 * Static variables (mapping)                                           *
 *======================================================================*/

/**
 * @brief Mapping from note (A to G) to tone (0-11).
 * @details C = 0 ... B = 11
 */
static uint8_t map_note_tone[]   = {
     9, // a
    11, // b
     0, // c
     2, // d
     4, // e
     5, // f
     7  // g
};

/**
 * @brief Mapping from tone (0=C4 to 11=B4) to pitch (in deciHz)
 */
static uint16_t map_tone_pitch[] = {
    2616, 2772,   // c, c#
    2936, 3111,   // d, d#
    3296,         // e
    3492, 3700,   // f, f#
    3920, 4153,   // g, g#
    4400, 4661,   // a, a#
    4938          // b
};



/*======================================================================*
 * Helper functions                                                     *
 *======================================================================*/

/**
 * @brief Convert tone/octave to pitch
 *
 * @note  Tone 0 at current octave (ie: 0) 
 *        is note C4 (scientifique notation) 261Hz.
 *
 * @param tone      relative tone
 * @param octave    relative octave
 *
 * @return pitch in Hz
 */
inline
uint16_t buzzer_convert_tone(int8_t tone, int8_t octave) {
    // Normalize tone and octave
    octave += tone / 12;
    tone   %= 12;
    if (tone < 0) {       // We need to fix things:
	tone    = -tone;  // - % is remains and not modulo
	octave -= 1;      // - we are dealing with signed integer
    }

    // Returns pitch in Hz
    return (octave >= 0 ? (map_tone_pitch[tone] <<   octave)
 	                : (map_tone_pitch[tone] >>  -octave)) / 10;
}


/**
 * @brief Convert a note/accidental/octave to pitch
 *
 * @note  Note c natural at current octave
 *        is note C4 (scientifique notation) 261Hz.
 *
 * @param note        BUZZER_NOTE_A to BUZZER_NOTE_G
 * @param accidental  BUZZER_NOTE_FLAT, BUZZER_NOTE_NATURAL, BUZZER_NOTE_SHARP
 * @param octave      relative octave
 *
 * @return pitch in Hz
 */
static inline
uint16_t note_pitch(uint8_t note, int8_t accidental, int8_t octave) {
    return buzzer_convert_tone(map_note_tone[note] + accidental, octave);
}


/**
 * @brief Convert a midi number to pitch
 *
 * @note  Midi number 60 is note C4 (scientifique notation) 261Hz.
 *
 * @param number      midi number (21 to 108)
 *
 * @return pitch in Hz
 */
static inline
uint16_t midi_pitch(uint8_t number) {
    return buzzer_convert_tone(number - 60, 0);
}


/**
 * @brief Convert a note value/dotted to a milliseconds length.
 *
 * @note  If bpm is null, will return BUZZER_LENGTH_INFINITE
 *
 * @param value   note value, expressed as 2^value
 * @param dot     number of dots
 *
 * @return milliseconds
 */
static inline
uint16_t note_length(uint8_t bpm, int8_t value, uint8_t dot) {
    // 1 bpm                = 60 * 1000 ms
    // 1 crotchet at 60 bpm = 1000 ms
    if (!bpm)
	return BUZZER_LENGTH_INFINITE;

    uint32_t ms = (60 * 1000) * 4;
    if (value < 0) { ms >>= -value; }
    else           { ms <<=  value; }
    ms /= bpm;
    if (dot)       { ms = (2 * ms) - (ms >> dot); }
    return ms;
}



/*======================================================================*
 * Melody                                                               *
 *======================================================================*/

#if BUZZER_USE_MELODY
static
char *melody_decode_silence(char *next, uint16_t *val,
			    uint8_t bpm, uint8_t ulength) {
    char c = *next++;
    // Zero silence
    if (c == 0) {
	*val = 0;
	
    // In 5 ms unit (from 1..128)
    } else if (c & 0x80) {
	*val = (((uint8_t)c) & 0x7F) * 5 + 5;
	
    // In milliseconds, 14bit encoded (+8bit extension)
    } else if (c & 0x40) {
	*val = ((c & 0x3F) << 8) | (*next++);
	if (*next == 0xFC) { // Check for 2bit extension
	    next++;
	    char e = *next++;
	    *val |= ((uint16_t)e) << 14;
	}
	
    // BPM relative (value/dotted) | Unit length
    } else {
	if (bpm) {
	    // Return 16bit value, but calculus are done 32bits
	    // Value is -8 .. +6  (0-15, 0 used for 0 length, do -9)
	    int8_t   value = ((c & 0x3F) >> 2) - 9;
	    uint8_t  dot   = c & 0x03;
	    uint32_t _ms   = 60*1000;
	    if (value < 0) { _ms >>= -value; }
	    else           { _ms <<=  value; }
	    _ms /= bpm;
	    if (dot)       { _ms = (2 * _ms) - (_ms >> dot); }
	    *val = _ms;
	}
	if (ulength) {
	    *val = ulength * (c & 0x3F);
	}
    }
    return next;
}


/**
 * @note No sanity check is performed during parsing, 
 *       String must have been validated before.
 * 
 */
static
void melody_next_I(buzzer_t buzzer) {
    buzzer_melody_ctx_t ctx = &buzzer->melody;
    uint16_t ms;
    uint16_t hz;
    char c, n;
    
 redo:
    c = *ctx->next++;
    // Command and control
    if (c > 0xCF) {
	switch(c) {
	// End of stream
	case 0xFF:
	    pwmDisableChannelI(buzzer->config->pwm, buzzer->config->channel);	
	    buzzer->done = true;
	    return; // Job's done!
        // Define BPM
	case 0xFE:
	    ctx->bpm     = (uint8_t) *ctx->next++;
	    ctx->ulength = 0;
	    break;
	// Define intersound silence
	case 0xFD: 
	    ctx->next = melody_decode_silence(ctx->next, &ctx->dflt_silence,
					      ctx->bpm, ctx->ulength);
	    break;
	// Silence extension
	case 0xFC: // It's used just after defining silence
	    break; // and it is decoding/consumed by the silence parser
	// Volume
	case 0xFB:
	    ctx->volume = *ctx->next++;
	    break;
        // Define Unit Length
	case 0xFA:
	    ctx->ulength = (uint8_t) *ctx->next++;
	    ctx->bpm     = 0;
	    break;
	}
	// Continue parsing
	goto redo;

    // Sound
    } else {
	// Sound encoded as frequency
	if (c & 0x80) {
	    hz = ((c & 0x7F) << 8) | (*ctx->next++);

	// Sound encoded as midi number
	} else {
	    hz = midi_pitch(c);
	}

	// Get next char (duration)
	n = *ctx->next++;

	// Duration
	// In milliseconds, 14bit encoded
	if (n & 0x40) {
	    ms = ((n & 0x3F) << 8) | (*ctx->next++);

	    
	// BPM relative (value/dotted) | Unit length
	} else {
	    if (ctx->bpm) {
		// Return 16bit value, but calculus are done 32bits
		int8_t   value = ((n & 0x3F) >> 2) - 9;
		uint8_t  dot   = n & 0x03;
		uint32_t _ms   = 60*1000;
		if (value < 0) { _ms >>= -value; }
		else           { _ms <<=  value; }
		_ms /= ctx->bpm;
		if (dot)       { _ms = (2 * _ms) - (_ms >> dot); }
		ms = _ms; // 16bits
	    }
	    if (ctx->ulength) {
		ms = ctx->ulength * (n & 0x3F);
	    }
	}

	// Silence
	if (n & 0x80) {
	    ctx->next = melody_decode_silence(ctx->next, &ctx->silence,
					      ctx->bpm, ctx->ulength);
	} else {
	    ctx->silence = ctx->dflt_silence;
	}
	
	// Come on honey, let's make some noise
	ctx->state = BUZZER_MELODY_SOUND;
	
	const pwmcnt_t period = buzzer->config->pwm->config->frequency / hz;
	pwmChangePeriodI(buzzer->config->pwm, period);
	pwmEnableChannelI(buzzer->config->pwm, buzzer->config->channel,
			  PWM_PERCENTAGE_TO_WIDTH(buzzer->config->pwm, 5000));
    }

    chVTSetI(&buzzer->vt, MS2ST(ms), buzzer_do_next, (void *)buzzer);	
}
#endif



/*======================================================================*
 * Buzzer scheduler                                                     *
 *======================================================================*/

static		     
void buzzer_do_next(void *args) {
    buzzer_t buzzer = (buzzer_t)args;

    chSysLockFromISR();
    
    if ((buzzer->done == true            ) ||
	(buzzer->type == BUZZER_TYPE_BEEP)) {
	pwmDisableChannelI(buzzer->config->pwm, buzzer->config->channel);	
	buzzer->done = true;
	
    } else if (buzzer->type == BUZZER_TYPE_RING) {
	uint16_t ms = 0;

	if (buzzer->ring.playing) {
	    buzzer->ring.playing = false;
	    pwmDisableChannelI(buzzer->config->pwm, buzzer->config->channel);	

	    if ((buzzer->ring.count > BUZZER_COUNT_MAX) ||
		(--buzzer->ring.count > 0)) {
		ms = buzzer->ring.delay;
	    } else {
		buzzer->done = true;
	    }
	} else {
	    buzzer->ring.playing = true;
	    pwmEnableChannelI(buzzer->config->pwm, buzzer->config->channel,
		      PWM_PERCENTAGE_TO_WIDTH(buzzer->config->pwm, 5000));
	    ms = buzzer->ring.length;
	}

	// If not done, schedule callback
	if (!buzzer->done)
	    chVTSetI(&buzzer->vt, MS2ST(ms), buzzer_do_next, args);
	
#if BUZZER_USE_MELODY	
    } else if (buzzer->type == BUZZER_TYPE_MELODY) {
	switch(buzzer->melody.state) {
	case BUZZER_MELODY_SOUND:
	    // No silence in-between
	    if (!buzzer->melody.silence)
		goto melody_next;
	    
	    // Shut up!
	    pwmDisableChannelI(buzzer->config->pwm, buzzer->config->channel);

	    // Enjoy the silence
	    buzzer->melody.state = BUZZER_MELODY_SILENCE;
	    chVTSetI(&buzzer->vt, MS2ST(buzzer->melody.silence), buzzer_do_next, args);
	    break;
	    
	case BUZZER_MELODY_SILENCE:
	melody_next:
	    melody_next_I(buzzer);
	    break;
	}
#endif
    }

    chSysUnlockFromISR();
}



/*======================================================================*
 * Buzzer functions                                                     *
 *======================================================================*/

/**
 * @brief Initialize buzzer driver
 *
 * @param buzzer      Buzzer context
 * @param cfg         Buzzer config
 */
void buzzer_init(buzzer_t buzzer, const BuzzerConfig *cfg) {
    buzzer->config  = cfg;
}


/**
 * @brief Turn off the buzzer
 * 
 * @param buzzer      Buzzer context
 */
void buzzer_off(buzzer_t buzzer) {
    // Lock
    osalSysLock();

    // Cancel pending buzzer operations
    chVTResetI(&buzzer->vt);
    pwmDisableChannelI(buzzer->config->pwm, buzzer->config->channel);	
    buzzer->done = true;

    // Unlock
    osalSysUnlock();
}


/**
 * @brief Play a musical note.
 * 
 * @param buzzer      Buzzer context
 * @param note        BUZZER_NOTE_A .. BUZZER_NOTE_G
 * @param accidental  BUZZER_NOTE_{FLAT,NATURAL,SHARP}
 * @param octave      octave offset
 * @param bpm         Beat per minute
 * @param value       Note value
 * @param dot         Number of dot
 */
void buzzer_note(buzzer_t buzzer,
		 uint8_t note, int8_t accidental, int8_t octave,
		 uint8_t bpm,  int8_t value,      uint8_t dot) {
    uint16_t hz       = note_pitch(note, accidental, octave);
    uint16_t length   = note_length(bpm, value, dot);
    buzzer_beep(buzzer, hz, length);
}

/**
 * @brief Play a midi defined number
 *
 * @param buzzer      Buzzer context
 * @param number      MIDI number
 * @param length      Sound length in milliseconds
 */
void buzzer_midi(buzzer_t buzzer, uint8_t number, uint16_t length) {
    uint16_t hz = midi_pitch(number);
    buzzer_beep(buzzer, hz, length);
}


/**
 * @brief Play a ringing sound
 *
 * @note  If hz, length, or count is zero, buzzer will be turned off.
 *
 * @note  If playing a ringing sound, length must be 
 *        in [0..BUZZER_LENGTH_MAX] and delay > 0.
 *        For more information take a look at BUZZER_LENGTH_* and
 *        BUZZER_DELAY_*
 *
 * @param buzzer      Buzzer context
 * @param hz          Pitch in HZ
 * @param length      Sound length in milliseconds
 * @param count       Number of time to play sound (BUZZER_COUNT_INFINITE)
 * @param delay       Delay in milliseconds between consecutive sounds
 */
void buzzer_ring(buzzer_t buzzer,
		 uint16_t hz,    uint16_t length,
		 uint16_t count, uint16_t delay) {
    // Sanity check
    osalDbgAssert(count <= 1 || length <= BUZZER_LENGTH_MAX,
		  "incompatible count/length");
    osalDbgAssert(count <= 1 || delay > 0,
		  "incompatible count/delay");
    
    // Lock
    osalSysLock();

    // Cancel pending buzzer operations
    chVTResetI(&buzzer->vt);
    
    // Guess type of sound: buzz, beep, or ring
    if      (count > 1) {                   buzzer->type = BUZZER_TYPE_RING; }
    else if (length <= BUZZER_LENGTH_MAX) { buzzer->type = BUZZER_TYPE_BEEP; }
    else {                                  buzzer->type = BUZZER_TYPE_BUZZ; }
	
    // Sound of silence?
    if (!hz || !length || !count) {
	pwmDisableChannelI(buzzer->config->pwm, buzzer->config->channel);	
	buzzer->done = true;
	goto leave;
    }

    // Cum on feel the noize!
    buzzer->done = false;
    const pwmcnt_t period = buzzer->config->pwm->config->frequency / hz;
    pwmChangePeriodI(buzzer->config->pwm, period);
    pwmEnableChannelI(buzzer->config->pwm, buzzer->config->channel,
		     PWM_PERCENTAGE_TO_WIDTH(buzzer->config->pwm, 5000));

    // Initialize timer callback 
    switch(buzzer->type) {
    default:
    case BUZZER_TYPE_BUZZ:
	// No callback for us, job's done.
	goto leave;

    case BUZZER_TYPE_BEEP:
	// Callback without args will turn off buzzer
	break;

    case BUZZER_TYPE_RING:
	// Callback with ring type argument
	buzzer->ring.length   = length;
	buzzer->ring.count    = count;
	buzzer->ring.delay    = delay;
	buzzer->ring.playing  = true;
	break;
    }

    // Schedule next buzzer event handling
    chVTSetI(&buzzer->vt, MS2ST(length), buzzer_do_next, (void*)buzzer);


 leave:
    // Unlock
    osalSysUnlock();
}


/**
 * @brief Play a melody (kakapo-encoded)
 *
 * @param buzzer      Buzzer context
 * @param melody      Melody data
 */
void buzzer_melody(buzzer_t buzzer, char *melody) {
    // Buzzer type
    buzzer->type           = BUZZER_TYPE_MELODY;

    // Initialize context
    buzzer_melody_ctx_t ctx = &buzzer->melody;
    ctx->bpm     = 120;
    ctx->ulength = 0;
    ctx->silence = 100;
    ctx->volume  = 100;
    ctx->next    = melody;

    // Start melody
    // Perform lock/unlock as melody_next_I is to be called from locked zone
    osalSysLock();
    buzzer->done = false;
    melody_next_I(buzzer);
    osalSysUnlock();
}

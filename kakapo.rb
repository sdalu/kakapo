# coding: utf-8

#
#    Copyright (C) 2016 Stephane D'Alu
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

begin require 'rubygems'      rescue LoadError end
begin require 'bundler/setup' rescue LoadError end

require 'csv'
require 'rbtree'
require 'set'
require 'prime'

######################################################################

class Kakapo
class Reader
    def initialize(*args, **opts)
    end
    def normalize(stream)
        stream
            .map {|freq,len|                    # Ensure in range freq/len
            # Frequency: 0, 20..20000
                freq = if    freq <= 0             then 0
                       elsif freq < FREQ_RANGE.min then FREQ_RANGE.min
                       elsif freq > FREQ_RANGE.max then FREQ_RANGE.max
                       else                             freq
                       end
                # Length: > 0
                raise "Negative length (for sound/silence)" if len <= 0
                # Final value (will return nil if discarding required)
                [ freq, len ] if freq && len }
            .compact                            # Remove discarded value
            .slice_when {|i, j| i[0] != j[0] }  # Chunk by equal frequency
            .map {|chunk| [ chunk.first[0],     # Aggregate chunk       # Freq
                            chunk.map {|freq, len| len }.reduce(:+) ] } # Len
    end
end
end



class Kakapo
    # Read file and return list of Frequence/Length
    def self.read(file, *args, **opts)
        k = case File.extname(file)[1..-1]
            when 'csv'  then Kakapo::Reader::CSV
            when 'beep' then Kakapo::Reader::Beep
            else raise "Unknown file format"
            end

        k.new(*args, **opts).parse(File.read(file)).to_a
    end
end



class Kakapo    
class Reader
# Beep parser
#
class Beep < Reader
    # Parse from a beep commmand line
    def parse(data)
        stream = data
            .gsub(/\\\r?\n/, '')     # One big line, oh yeah, oh yeah
            .gsub(/^(?:.*beep)?/,'') # Only keep arguments
            .split('-n')             # Split between beeps
            .map {|beep|             # Convert beep string to key/value group
                Hash[beep.scan(/(-(\w)\s*(\d+(?:\.\d+)?))/)
                         .map {|_, k, v|
                             [k.to_sym, case k
                                        when 'f' then v.to_f
                                        else          v.to_i
                                        end] }
                    ] }
            .map {|beep|             # Inject default values
                { f: 440, l: 200, r: 1, d: 100, D: nil }.merge(beep) }
            .flat_map {|beep|        # Convert to frequency/length tuples
                l  = [ [ beep[:f], beep[:l] ] ]
                l *= beep[:r]
                d  = beep[:D] || beep[:d]
                if d > 0
                    l = l.flat_map { |x| [x, [0, d] ]}
                    l.pop unless beep[:D]
                end
                l
            }
        normalize(stream)
    end    
end

# CVS Parser
#
class CSV < Reader
    # Parse from a CSV 
    def parse(data)
        stream = ::CSV.parse(data).map { |frequency, duration|
            [ frequency.to_f, duration.to_i ]
        }
        normalize(stream)
    end
end
end
end


######################################################################


class Kakapo
    # Range constants
    MIDI_N_RANGE = 21..108     # Midi number
    FREQ_RANGE   = 20..20000   # Sound frequency
    VALUE_RANGE  = -8..6       # Note value
    DOT_RANGE    =  0..3       # Note number of dots
    BPM_RANGE    =  1..255     # Beat per minute
    UCOUNT_RANGE =  1..63      # Nomber of Unit length
    ULEN_RANGE   =  1..250     # Length of a unit
    
    # Mapping between midi numbers and frequencies
    MIDI_N2F = Hash[MIDI_N_RANGE.map{|n| [n, (2**((n-69)/12.0))*440]}]

    # Mapping between frequencies and midi numbers
    MIDI_F2N = RBTree[*MIDI_N_RANGE.flat_map{|n| [(2**((n-69)/12.0))*440, n] }]

    # Mapping between milliseconds and bpm/value/dot notation
    MS2BVD   = RBTree.new
    BPM_RANGE.each {|b|
        VALUE_RANGE.each {|v|
            DOT_RANGE.each {|d|
                ms = (60000.0 / b) * 2**v
                ms = (2*ms) - ms / 2**d
                (MS2BVD[ms.round] ||= []) << [b, v, d]
            }
        }
    }
    
    # Return a list of matching <bpm/value/dot> for the given
    # length (duration)
    def self.bvd_matching(length, delta_error: 2, pct_error: 1)
        # Compute lower/upper boundaries according to error
        lower, upper  = [[], []]
        if delta_error
            lower << length - delta_error
            upper << length + delta_error
        end
        if pct_error
            lower << length * (100-pct_error)/100
            upper << length * (100+pct_error)/100
        end

        # Retrieve list of matching <bpm/value/dot> for the given length
        MS2BVD.bound(lower.max||length, upper.min||length)
              .flat_map {|k, v| v }
    end

    # Give an approximation of the length, in length unit
    #
    # @return Integer        the number of unit
    # @return nil            if no approximation found
    def self.ulength(ulength, length, delta_error: 2, pct_error: 1)
        return unless ulength                    # We don't have unit length
        length = length.to_f                     # Ensure float for calculus
        units  = (length / ulength).ceil         # Value approximation
        return unless (units == 0) ||            # Out of range
                      UCOUNT_RANGE.include?(units) 
        units                                    # Returns unit count
    end

    # Give an approximation of the length, in Value/Dot (for the given BPM)
    #
    # @return Array<Integer> the value/dot approximation
    # @return nil            if no approximation found
    def self.value_dot(bpm, length, delta_error: 2, pct_error: 1)
        return unless bpm                        # We don't have a BPM
        length = length.to_f                     # Ensure float for calculus
        unit  = 60000.0 / bpm                    # Time unit from BPM
        value = Math.log2(length / unit).floor   # Value approximation
        return unless VALUE_RANGE.include?(value)# Is approximation in range?
        ms    = unit * 2**value                  # ms as bpm/value
        dot   = -Math.log2(2-(length/ms)).round  # Dot approximation
        dot   = [ dot, DOT_RANGE.max ].max       # Control dot value
        ms    = 2*ms - ms / 2**dot               # ms as bpm/value/dot
        edlt  = (ms - length).abs                # Error 
        epct  = edlt * 100 / length              # Error in percentage
        return if delta_error&.< edlt            # Ensure acceptable error
        return if pct_error&.< epct              # Ensure acceptable error
        [ value, dot ]                           # Returns value/dot
    end
    
    # Convert frequency to midi number
    def self.midi_number(freq, pct_error: 2, rounding: -1)
        # Convert to midi number
        freq     = freq.to_f
        number   = (12 * Math.log2(freq/440.0) + 69).round
        # Ensure we got a midi number
        return nil unless MIDI_N_RANGE.include?(number)
        # Ensure we are not too for from reality
        realfreq = MIDI_N2F[number]
        return nil if pct_error &&
                      ((freq - realfreq) * 100 / realfreq).abs > pct_error
        return nil if rounding  &&
                      (freq.round(rounding) != realfreq.round(rounding))
        # All good, return midi number
        return number
    end

    # Silence guessing
    def self.silence_guessing(song)
        s = song.select {|frequency,| frequency < FREQ_RANGE.min }
                .map {|_, length| length }
        if s.size < (song.select{|freq,| FREQ_RANGE.include?(freq) }.size/2)
            0
        else
            s.each_with_object(Hash.new(0)) { |len,counts| counts[len] += 1 }
             .sort {|a,b| b[1] <=> b[0] }
             .first[0]
        end
    end

    # Length unit guessing
    # TODO: improve guessing when silence
    def self.unit_length_guessing(song)
        song = song.select {|frequency, | FREQ_RANGE.include?(frequency) }
        ulen = song.map {|_, length| length}.reduce(:gcd)
        return unless ULEN_RANGE.include?(ulen)

        #Prime.prime_division(20).flat_map { |factor, power| [factor] * power }
        #.combiation
        
        # Check that all length can be encoded
        if song.all? {|_, length| u = (length/ulen).ceil
               UCOUNT_RANGE.include?(u) || (u==0)
           }
            ulen
        end
    end

    
    # BPM guessing
    def self.bpm_guessing(song)
        songsize   = song.size
        scoreboard = Hash.new(0)
        
        # Set of possibly matching bpm for every sound
        bpms_per_sound = song.map {|frequency, length|
            Set.new(Kakapo.bvd_matching(length).map {|bpm,| bpm })
        }
        
        # List of possible bpm for the song
        bpms = bpms_per_sound.reduce(:+)
        
        # Set scoreboard
        bpms.each {|bpm| scoreboard[bpm] = 1.0 }
        
        # Occurence of bpm in the song
        coverage = Hash.new(0)
        bpms_per_sound.select {|bpm_set|
            bpm_set.each {|bpm| coverage[bpm] += 1 }
        }
        
        # Update scoreboard
        coverage.each {|bpm, count|
            scoreboard[bpm] *= count.to_f / songsize }
        
        # Compute streaks for every bpm
        streaks = Hash[
            bpms.map {|bpm|
                streak = bpms_per_sound.map {|set| set.include?(bpm) }
                                       .slice_when {|i, j| i != j }
                                       .map {|streak| streak.size }
                streak.unshift(0) unless bpms_per_sound.first.include?(bpm)
                [ bpm, streak ]
            }]
        
        # Update scoreboard
        streaks_count = streaks.map {|bpm, streak|  streak.size }.max
        streaks.each {|bpm, streak|
            scoreboard[bpm] *=  1 - ((streak.size.to_f)/streaks_count)/2
        }
        
        # Compute occurences of < BPM / Value / Dot >
        bvd_occurence = Hash.new(0)
        song.each {|frequency, length|
            Kakapo.bvd_matching(length).each {|bvd|
                bvd_occurence[bvd] += 1
            }
        }
        
        # Update scoreboard according to Value/Dot
        bpms.each {|bpm|
            dot    = Hash.new(0)
            value  = Hash.new(0)
            bvd_occurence
                .select { |bvd, | bvd.first == bpm }
                .each   { |bvd, count | value[bvd[1]] += count
                dot  [bvd[2]] += count }
            score  = 1
            score *= 1 - (dot[1].to_f/songsize)/8
            score *= 1 - (dot[2].to_f/songsize)/4
            score *= 1 - (dot[3].to_f/songsize)/2
            score *= (value[0]+value[-1]+value[-2]+value[-3]+value[-4])/songsize.to_f
            scoreboard[bpm] *= score
        }

        
        scoreboard
            .sort {|a,b| b[1] <=> a[1] }
            .map  {|bpm, p| [ bpm, { :coverage => coverage[bpm].to_f/songsize,
                                     :proba    => p } ] }
    end

end


######################################################################


class Kakapo    
class Encoder
    def initialize(bpm: nil, ulength: nil, silence: nil)
        @length_error_pct    = 1
        @frequency_error_pct = 1
        @bpm                 = bpm
        @ulength             = ulength
        @silence             = silence || 100
        if bpm && ulength
            raise "BPM and ulength can't be defined booth"
        end
    end

    def encode(song)
        e = []
        e << encode_bpm(@bpm)                   if @bpm
        e << encode_ulength(@ulength)           if @ulength
        e << encode_default_silence(@silence)

        while data = song.shift
            frequency, length = data
            if frequency == 0
                raise "silence must occured after a sound"
            end
            silence = if !song.empty? && (song.first[0] == 0)
                      then song.shift[1]
                      else 0
                      end
            e << encode_note(frequency, length, silence)
        end
        e << encode_end
        e.join
    end

    private

    def ulength(len)
        Kakapo.ulength(@ulength, len) if @ulength
    end
    
    def value_dot(len)
        Kakapo.value_dot(@pbm, len, pct_error: @length_error_pct) if @bpm
    end

    def encode_volume(vol)
        if (1..100).include?(vol)
            raise "volume must be between 1..100"
        end
        [ 0xF9, vol ].pack('CC')
    end

    def encode_end
        [ 0xFF ].pack('C')
    end

    def encode_bpm(bpm)
        [ 0xFE, bpm ].pack('CC')
    end

    def encode_default_silence(length)
        [ 0xFD ].pack('C') + encode_silence(length)
    end

    def encode_ulength(ulength)
        [ 0xFA, ulength ].pack('CC')
    end

    

    
    def encode_note(frequency, length, silence)
        # Flags
        flags  = 0

        # Frequency
        freq   = if n = Kakapo.midi_number(frequency,
                                           pct_error: @frequency_error_pct)
                 then [n].pack('C')
                 else [frequency.round | 0x8000].pack('S>')
                 end

        # Explicite silence
        es     = if silence != @silence
                     flags |= 0x80
                     encode_silence(silence)
                 else
                     ""
                 end
        
        # Try encoding using value/dot
        if vd = value_dot(length)
            value, dot = vd
            return freq + [flags | ((value+9) << 2) | dot].pack('C') + es
        end

        # Try encoding using unit length
        if u = ulength(length)
            return freq + [flags | u].pack('C') + es
        end

        # Try in ms on 14bit word
        if length < 16383
            return freq + [(flags << 8) | length | 0x4000].pack('S>') + es
        end

        # That's too big
        raise "We don't support more that 16sec (unless bpm/value/dot)"
    end

    def encode_silence(length)
        # Special case
        if length == 0
            return [0].pack('C')
        end

        # Try encoding using value/dot
        if vd = value_dot(length)
            value, dot = vd
            return [((value+9) << 2) | dot].pack('C')
        end
        
        # Try encoding in 5ms unit
        count = (length.to_f / 5).round
        epct  = ((length.to_f - 5*count) * 100 / length).abs
        if (1..128).include?(count) && (epct < @length_error_pct)
            return [(count-1) | 0x80].pack('C')
        end

        # Try in ms on 14bit word
        if length < 16383
            return [length | 0x4000].pack('S>')
        end

        # Try in ms on a 16bit, That's a big silence
        if length < 65535
            return [(length & 0x3FFF)|0x4000, 0xFC, length >> 14].pack('S>CC')
        end

        # That's fucking big
        raise "We don't support more that 1 minute of silence"
    end
end
end

######################################################################


require 'optparse'


$opts = { :format => :C }
OptionParser.new do |opts|
    opts.banner = "Usage: kakapo file [options]"

    opts.on("-v", "--[no-]verbose",             "Run verbosely"    ) do |v|
        $opts[:verbose] = v
    end

    opts.on("-b", "--bpm BPM",        Integer,  "Beat Per Minute"  ) do |v|
        $opts[:bpm    ] = v
    end

    opts.on("-u", "--ulen LENGTH",    Integer,  "Unit length 1..200ms") do |v|
        if !Kakapo::ULEN_RANGE.include?(v)
            raise "Bad unit length value"
        end
        $opts[:ulength] = v
    end

    opts.on("-s", "--silence LENGTH", Integer,  "Inbetween silence") do |v|
        $opts[:silence] = v
    end

    opts.on("-f", "--format FORMAT", [ :C ],    "Output format"    ) do |v|
        $opts[:format ] = v
    end

    opts.on("-o", "--output FILENAME",          "Output filename"  ) do |v|
        $opts[:output ] = v
    end

    opts.on("-w", "--wrap PREFIX",              "Prefix for output") do |v|
        $opts[:wrap   ] = v
    end

    opts.on("-h", "--help",                     "Prints this help" ) do
        puts opts
        exit
    end
end.parse!

if ARGV.size != 1
    $stderr.puts "Input filename missing"
    exit
end



$k = Kakapo.new
$f = ARGV[0]
$s = Kakapo.read($f)

silence = $opts[:silence] || Kakapo.silence_guessing($s)
bpm     = $opts[:bpm]     || if (bpm_guess = Kakapo.bpm_guessing($s).first) &&
                                (bpm_guess[1][:proba   ] > 0.95)            &&
                                (bpm_guess[1][:coverage] > 0.90)
                                 bpm_guess[0]
                             end
ulength = $opts[:ulength] || Kakapo.unit_length_guessing($s)

if $opts[:verbose]
    puts "Song size     = #{$s.size}"
    puts "Using BPM     = #{bpm}"
    puts "Using Unit    = #{ulength}"
    puts "Using silence = #{silence}"
end

if    bpm.nil? && ulength.nil?
    warn "Unable to guess BPM or Unit length (fallback to full length coding)"
elsif bpm && ulength
    if $opts[:verbose]
        puts "==> Using Unit-encoding as it is cheaper on decoder side"
    end
    bpm = nil   # Both encoding, possible, ulength is less CPU costly
end


$e   = Kakapo::Encoder.new(:bpm => bpm, :ulength => ulength,
                           :silence => silence)
data = $e.encode($s)

if $opts[:verbose]
    puts "==> Encoded size: #{data.size}"
end

data = case $opts[:format]
       when :C
           d = [ data
                   .each_byte.map {|b| "0x%02x" % [b] }
                   .each_slice(8).map{|s| s.join(', ') }
                   .join(", \n") ]
           if $opts[:wrap]
               n = File.basename($f, ".*")
               d.unshift("char #{$opts[:wrap]}_#{n}[] = {")
               d.push("};")
           end
           d.join("\n")
       end

if $opts[:output]
    File.write($opts[:output], data + "\n")
else
    puts data
end

